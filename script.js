console.log('test');
/*
// Arrays - used to store multiple related values in a single variable
- declared using square brackets [] known as array literals

elements - are values inside an array

index - 0 = offset
element 1 = index 0

Syntax: let/const arrayName = [elementA, elementB,....]


*/


let grades = [98, 94, 89, 90]
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

console.log(grades);
console.log(computerBrands);

let myTasks =['bake sass', 'drink html', 'inhale css', 'eat javascript'];

console.log(myTasks);

myTasks[0] = 'hello world';

console.log(computerBrands.length);

/*
1. Mutator Methods - to change an array

//- push: adds an element in the end of the array and return its length
// Syntax: arrayName.push(ElementA, elementB, ...);
*/
let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];
console.log('Current Array');
let fruitsNew = fruits.push('Banana');
console.log(fruits);

let favFruit = fruits.push('Rambotan');
console.log(fruits);

//pop - removes the last element in an array and return the removed element

fruits.pop();
console.log(fruits);

//shift - removes an element at the beginning of an array and return the removed element
let firstElement = fruits.shift();
console.log(fruits);
console.log(firstElement);

//unshift - adds one or more element/s at the beginning of an array
fruits.unshift('Lime', 'Banana');
console.log(fruits);

//splice - removes element from specified index and add new elements
//Syntax: arrayName.splice(startingIndex, deleteCount,ElementsToBeAdded);
//it will count the index from the starting Index, from there it will delete a number of elements in the index
console.log('test splice');
fruits.splice(1, 2, 'Cherry', 'Grapes');
console.log('after splice');
console.log(fruits);

//sort - rearranges the elements in alphanumeric order
fruits.sort();
console.log(fruits);

//reverse - reverses the order of an array
console.log('reverse');
fruits.reverse();
console.log(fruits);

/*
2. Non Mutator Methods - do not modify or change an array
*/
let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'FR', 'DE'];
console.log(countries);

//indexof()
//-returns the first index of the first matching element found in an array
// if no match is found, it returns -1

let firstIndex = countries.indexOf('PH');
console.log('Result of indexOf() method:' +firstIndex);

let invalidCountry = countries.indexOf('BR');
console.log('Result of indexOf() method:' +invalidCountry)

//lastIndex()
//-returns the last matching element found in an array
//syntax: arrayName.lastIndexOf(searchValue);
//search in lastindexof starts with the last index to start

let lastIndex = countries.lastIndexOf('PH');
console.log("Result of indexOf() method:" +lastIndex)

let lastIndexStart = countries.lastIndexOf('PH', 2);
console.log("Result of indexOf() method:" +lastIndex)

//slice - slices a portion of an array and return a new array
//syntax arrayName.slice(startingIndex);
//syntax arrayname.slive(startingIndex, endingIndex);
let slicedArrayA = countries.slice(2);
console.log('results from slice A');
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2, 4);
console.log('results from slice B');
console.log(slicedArrayB);

//slices from the last element and then finishes
let slicedArrayC = countries.slice(-3);
console.log('results from slice C');
console.log(slicedArrayC);

//toString
let stringArray = countries.toString();
console.log("to string results:" +stringArray);

//concat - combines two or more arrays and return the combined result
let taskArrayA = ['drinkHTML', 'eat Javascript'];
let taskArrayB = ['inhale css', 'breathe sass'];

let tasks = taskArrayA.concat(taskArrayB);
console.log(tasks)

//join - returns an array as string seperated by specified seperator
let users = ['John', 'Jane', 'Joe', 'Robert'];
sample = users.join('*');
console.log(sample);

/*
3. Iteration Methods - loops designed to perform repetitive tasks on arrays
*/

//foreach
//syntax:

countries.forEach(function(country){
	console.log(country)
})

console.log('sss');
countries.forEach(function(country){
	if(country.length > 10){
		console.log(country)
	}
})

//map - iterates on each element and returns new array with different values depending on the results of the function's operation

let numbers = [1,2,3,4,5];

let numberMap = numbers.map(function(numbers){
	return numbers * numbers;
})

console.log('Result from map()');
console.log(numberMap);

//every

let allValid = numbers.every(function(number){
	return(number <3);
})
console.log('Result from every()');
console.log(allValid);

//some

let someValid = numbers.some(function(number){
	return(number <3);
})
console.log('Result from some()');
console.log(someValid);

//filter
let filterValid = numbers.filter(function(number){
	return(number<3);
})
console.log('Result from filter()');
console.log(filterValid);

//includes
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let filterProducts = products.filter(function(products){
	return products.toLowerCase().includes('a');
})
console.log('Result from filter() & includes');
console.log(filterProducts);

/*
//reduce
// let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
	statement
})*/


//Multi-dimensional Array
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard[7][2]);